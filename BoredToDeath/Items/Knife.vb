﻿Public Class Knife
    Inherits Item

    Public Sub New()
        name = "Knife"
        icon = My.Resources.Knife
        isUsable = False
        dangerVal = 50
        funVal = 27
        description = vbCrLf & "A sharp metallic utensil.  This one is made for cooking." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You use the knife to cut up anything you can find.  Shreds of fabric, paper, and food now litter the house."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "As your playing around with the knife, you hit a nearby surface and it bounces back into your neck..."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
