﻿Public Class StuffedAnimal
    Inherits Item

    Public Sub New()
        name = "StuffedAnimal"
        icon = My.Resources.StuffedAnimal
        isUsable = True
        dangerVal = 2
        funVal = 10
        description = vbCrLf & "Probably one of your sister's teddy bears. Wait, you don't have a sister." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "If you're playing this game, then you're probably too old to play with this guy.  But just look at his puffy wuffy face!"
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Coming to life (as all stuffed animals do in the dark) the teddy bear stabs you 47 times in the chest and stomach area."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class