﻿Public Class Magazine
    Inherits Item

    Public Sub New()
        name = "Magazine"
        icon = My.Resources.Magazine
        isUsable = True
        dangerVal = 3
        funVal = 30
        description = vbCrLf & "No, it's not porn.  It looks like an issue of Reader's Digest." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "Turns out it accually WAS porn.  Huh..."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You get a paper cut, it rapidly becomes infected, and you die.  What a way to go..."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class

