﻿Public Class Lighter
    Inherits Item

    Public Sub New()
        name = "Lighter"
        icon = My.Resources.Lighter
        isUsable = True
        dangerVal = 20
        funVal = 60
        description = vbCrLf & "Hmm, a lighter.  I see possibilities..." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You use the lighter to burn numerous things around the room until it eventually runs out of fuel.  Nice."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Let's just say that more things caught fire than should have... including yourself..."
            Boredom = 100 'This makes you die, don't change this
            Status = 1
        End If

        Return result
    End Function
End Class
