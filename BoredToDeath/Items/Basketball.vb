﻿Public Class basketball
    Inherits Item

    Public Sub New()
        name = "Basketball"
        icon = My.Resources.Basketball
        isUsable = True
        dangerVal = 5
        funVal = 15
        description = vbCrLf & "It's a basketball.  Maybe you'll become the next Shaq." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You dribble the basketball for a while.  Then you realize that you'll never be Shaq..."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "The basketball literally breaks at the seams and a jet stream of air shoots up your nose, causing fatal brain damage."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
