﻿Public Class EnergyDrink
    Inherits Item

    Public Sub New()
        name = "EnergyDrink"
        icon = My.Resources.EnergyDrink
        isUsable = True
        dangerVal = 10
        funVal = 30
        description = vbCrLf & "1000 mg Caffeine and a bunch of Taurine." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You level of focus is so clean and intense that it can only be referred to as Nirvana (not the band)."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Upon drinking, you forget that you have a coronary heart condition. The irregular heartbeat is borderline tachycardia."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
