﻿Public Class Food
    Inherits Item

    Public Sub New()
        name = "Food"
        icon = My.Resources.Food
        isUsable = False
        dangerVal = 2
        funVal = 20
        description = vbCrLf & "It's too dark to see what kind of food it is..." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "It was just a thick cut of juicy steak. Delicious!  Yet, not too interesting."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "That's no food.  You ate the cat.  One of the bones punctured your stomach and you die a slow death."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class

