﻿Public Class Bleach
    Inherits Item

    Public Sub New()
        name = "Bleach"
        icon = My.Resources.Bleach
        isUsable = False
        dangerVal = 65
        funVal = 5
        description = vbCrLf & "Smells like death. Tastes like burning." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You clean the whole house. The fumes don't kill you."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You clean the whole house. The fumes kill you."
            Boredom = 100 'This makes you die, don't change this
            Status = 2
        End If

        Return result
    End Function
End Class
