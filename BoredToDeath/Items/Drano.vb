﻿Public Class Drano
    Inherits Item

    Public Sub New()
        name = "Drano"
        icon = My.Resources.Drano
        isUsable = False
        dangerVal = 80
        funVal = 4
        description = vbCrLf & "The liquid plumber. Don't breathe it." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You unclog the mess in your bathroom sink from that one time with the accident and the grossness."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You breathed it, it sends you into a hallucinogenic stupor where you die spinning around on the ground naked."
            Boredom = 100 'This makes you die, don't change this
            Status = 2
        End If

        Return result
    End Function
End Class
