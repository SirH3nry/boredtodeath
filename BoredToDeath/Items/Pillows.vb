﻿Public Class Pillows
    Inherits Item

    Public Sub New()
        name = "Pillows"
        icon = My.Resources.Pillows
        dangerVal = 3
        funVal = 10
        description = vbCrLf & "They look as soft as a cloud." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You make a pillow fort.  Cool."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You smothered yourself with the pillows.  Yeah, don't ask me."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class