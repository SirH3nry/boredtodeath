﻿Public Class Gun
    Inherits Item

    Public Sub New()
        name = "Gun"
        icon = My.Resources.Gun
        isUsable = False
        dangerVal = 55
        funVal = 55
        description = vbCrLf & "A self defense pistol.  Your dad keeps it around, just in case." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You spot several breakable objects (and expensive) objects around the house.  You use them for target practice, making quite a lot of noise."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You twirl the gun around your index finger.  You didn't pay attention to your dad when he taught you gun safety however, and you didn't check the safety or chamber.  Your finger hits the trigger in just the right way as your head turns into many bits throughout the room."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
