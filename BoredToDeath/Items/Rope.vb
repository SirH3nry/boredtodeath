﻿Public Class Rope
    Inherits Item

    Public Sub New()
        name = "Rope"
        icon = My.Resources.Rope
        isUsable = True
        dangerVal = 10
        funVal = 50
        description = vbCrLf & "Grade A rope with two ends and a middle in between!" & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You do everything you can think off: jump rope, wrangling wild beasts, tight-rope; but it's just not that interesting."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Somehow, someway, you manage to hang yourself.  Crap."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class