﻿Public Class Stereo
    Inherits Item

    Public Sub New()
        name = "Stereo"
        icon = My.Resources.Stereo
        dangerVal = 10
        funVal = 25
        description = vbCrLf & "A old stereo.  It plays music, although the power is out..." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You realize that the stereo can be battery operated and still has batteries in it.  You jam out to some old 90's CDs that were left in it."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You realize that the stereo can be battery operated and still has batteries in it.  You turn on the stereo before checking the volume and the intense volume startles you as you trip an fall on a sharp object, impaling you..."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
