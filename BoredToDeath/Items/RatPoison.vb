﻿Public Class RatPoison
    Inherits Item

    Public Sub New()
        name = "RatPoison"
        icon = My.Resources.RatPoison
        isUsable = True
        dangerVal = 80
        funVal = 5
        description = vbCrLf & "Has a big 'Mister Yuck' sticker on it." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You put some on a piece of cheeze and a cute baby rat comes out of the wall and eats it. The rat dies. You feel terrible."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "What good did you think could come of rat poison? You somehow ingest some, hallucinate, then die."
            Boredom = 100 'This makes you die, don't change this
            Status = 2
        End If

        Return result
    End Function
End Class
