﻿Public Class Bullets
    Inherits Item

    Public Sub New()
        name = "Bullets"
        icon = My.Resources.Bullets
        isUsable = False
        dangerVal = 40
        funVal = 20
        description = vbCrLf & "Useless without a gun... Right?" & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You throw them at things."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You throw a bullet at the wall, it bounces off and hits another wall, then bounces into your jugular."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
