﻿Public Class SteveCat
    Inherits Item

    Public Sub New()
        name = "Cat"
        icon = My.Resources.steveCat
        isUsable = True
        dangerVal = Rnd() * 100 + 1
        funVal = 100
        description = vbCrLf & "As you gaze upon it, it gazes back into you... " & " (Danger = " & "???" & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "Oddly enough, your boredom vanished... Steve, you've done it again!"
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Sooooo yeeah... you died..."
            Boredom = 100 'This makes you die, don't change this
            Status = 3
        End If

        Return result
    End Function
End Class
