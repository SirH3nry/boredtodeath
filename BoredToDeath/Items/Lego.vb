﻿Public Class Lego
    Inherits Item

    Public Sub New()
        name = "Lego"
        icon = My.Resources.Lego
        dangerVal = 5
        funVal = 50
        description = vbCrLf & "A small 4x2 lego brick.  It can be used to build many things." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You pretend the lego is many random objects until you eventually lose interest in the fantasy world, as well as the brick itself."
            Boredom -= funVal
        Else
            'If you're unlucky, this is displayed
            result = "As you are fumbling with the lego, you drop it and lose sight of it.  After a few steps, the brick lodges itself into the center of your foot as you fall over and bash your head of the nearby furniture."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
