﻿Public MustInherit Class Item

    Public name As String
    Public icon As Image        'null if cant be placed in inventory
    Public isUsable As Boolean  'specifies if an item can be picked up and used from inventory
    Public dangerVal As Integer 'The number (in 10ths of a percent) that determines if you die
    Public funVal As Integer    'The amount your boredom decreases when used
    Public description As String

    Public MustOverride Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
    Public Function dangerCalc(ByVal dangerVal As Integer)
        Dim OK As Boolean = True
        Dim temp As Integer

        temp = Int(Rnd() * 100 + 1)
        If dangerVal >= temp Then
            OK = False
        End If

        Return OK
    End Function

End Class
