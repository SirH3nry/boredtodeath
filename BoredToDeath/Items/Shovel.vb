﻿Public Class Shovel
    Inherits Item

    Public Sub New()
        name = "Shovel"
        icon = My.Resources.Shovel
        dangerVal = 10
        funVal = 25
        description = vbCrLf & "A pretty standard metal shovel." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You start swinging the shovel around aimless as if it were a sword.  Many pieces of valuable kitchenware shatters on the ground.  That was fun."
            Boredom -= funVal
        Else
            'If you're unlucky, this is displayed
            result = "You decide to go to the backyard and dig around for treasure.  Instead you dig right into a sinkhole and get buried alive."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
