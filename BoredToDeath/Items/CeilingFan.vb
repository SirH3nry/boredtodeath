﻿Public Class CeilingFan
    Inherits Item

    Public Sub New()
        name = "CeilingFan"
        icon = My.Resources.CeilingFan
        isUsable = False
        dangerVal = 10
        funVal = 10
        description = vbCrLf & "The ceiling fan spins slowly on the... ceiling." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "It turns out that a light breeze from a ceiling fan doesn't provide too much entertainment..."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "The ceiling fan begins revolving at an unprecedented speed.  It falls from the ceiling into your tender temple..."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
