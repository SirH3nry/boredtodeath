﻿Public Class Lamp
    Inherits Item

    Public Sub New()
        name = "Lamp"
        icon = My.Resources.Lamp
        isUsable = False
        dangerVal = 4
        funVal = 5
        description = vbCrLf & "A fairly modern looking house lamp.  Normally it would be used to provide some light." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You stare at the lamp hoping something interesting will happen.  Then you realize that lamps are not very fun."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Hoping to amuse yourself, you throw the lamp at the wall.  It shatters into pieces, one of which comes back, goes through your eye and lodges into your brain..."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
