﻿Public Class RollerSkates
    Inherits Item

    Public Sub New()
        name = "Skates"
        icon = My.Resources.Rollerskates
        isUsable = True
        dangerVal = 45
        funVal = 30
        description = vbCrLf & "Your Dad's prized possessions from his Glory Days." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You do a couple laps around the room until you decide it would be much more fun to strap the cat to them..."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You roller skate straight towards the window and fall head first-through; breaking your neck upon impact."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
