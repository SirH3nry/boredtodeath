﻿Public Class Matches
    Inherits Item

    Public Sub New()
        name = "Matches"
        icon = My.Resources.Matches
        isUsable = True
        dangerVal = 10
        funVal = 7
        description = vbCrLf & "A small package of 10 matches." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You light a match... it burns for a couple seconds. Wooh."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You catch yourself on fire."
            Boredom = 100 'This makes you die, don't change this
            Status = 1
        End If

        Return result
    End Function
End Class
