﻿Public Class CD
    Inherits Item

    Public Sub New()
        name = "CD"
        icon = My.Resources.CD
        isUsable = False
        dangerVal = 8
        funVal = 15
        description = vbCrLf & "I liked the floppy disk, personally." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You throw the disk at a wall and watch it shatter into many pieces."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Somehow the disk breaks in half and slits your throat."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
