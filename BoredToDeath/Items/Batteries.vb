﻿Public Class Batteries
    Inherits Item

    Public Sub New()
        name = "Batteries"
        icon = My.Resources.Batteries
        isUsable = False
        dangerVal = 25
        funVal = 2
        description = vbCrLf & "A wide variety of batteries, AA, AAA, 9V, C, D, A23, CR1216." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You use the 9-volt to complete a circuit with a potato."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "The battery your are holding breaks in half and sprays acid into your face."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
