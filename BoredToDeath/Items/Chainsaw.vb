﻿Public Class Chainsaw
    Inherits Item

    Public Sub New()
        name = "Chainsaw"
        icon = My.Resources.Chainsaw
        isUsable = True
        dangerVal = 95
        funVal = 70
        description = vbCrLf & "I hear that becoming a Lumberjack is all the rage nowadays." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "Blatantly sawing things in half turns out to be a LOT of fun!"
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Jason came in. He showed you how much bigger of a chainsaw he had. Then he cut you in half."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class