﻿Public Class Sink
    Inherits Item

    Public Sub New()
        name = "Sink"
        icon = My.Resources.Sink
        dangerVal = 20
        funVal = 10
        description = vbCrLf & "For some unexplainable reason, you felt the need to detach the kitchen sink and carry it with you." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "The detached kitchen sink was less fun then your previous thoughts,  although you reminisce about the good times destroying the ktichen to take it out."
            Boredom -= funVal
        Else
            'If you're unlucky, this is displayed
            result = "While carrying the sink around, the weight catches up with you as you drop it.  It puts a hole in the floor and you fall in and hit your head of the sink."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
