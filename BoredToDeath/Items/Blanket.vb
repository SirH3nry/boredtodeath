﻿Public Class Blanket
    Inherits Item

    Public Sub New()
        name = "Blanket"
        icon = My.Resources.Blankets
        isUsable = False
        dangerVal = 3
        funVal = 20
        description = vbCrLf & "Warm and cozy." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You make a blanket fort reminiscent of your younger days."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You get under the blanket and can't find your way out. You suffocate."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
