﻿Public Class Beer
    Inherits Item

    Public Sub New()
        name = "Beer"
        icon = My.Resources.Beer
        isUsable = True
        dangerVal = 50
        funVal = 70
        description = vbCrLf & "A 6-pack. If you drink this, fun will follow... Probably" & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You drink it in two quick chugs. feelsgoodman.jpg"
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Upon drinking, you forget that handle of vodka you buttchugged earlier. You pass out and die."
            Boredom = 100 'This makes you die, don't change this
            Status = 2
        End If

        Return result
    End Function
End Class
