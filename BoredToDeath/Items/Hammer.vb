﻿Public Class Hammer
    Inherits Item

    Public Sub New()
        name = "Hammer"
        icon = My.Resources.Hammer
        isUsable = False
        dangerVal = 13
        funVal = 15
        description = vbCrLf & "A typical hammer used to drive in nails and possibly pull them out." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You use the hammer to smash random objects around the house.  It was mildly entertaining."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "While swinging around the hammer, the head breaks away and hits you right in the skull, causing you do die slowly due to head trauma."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class

