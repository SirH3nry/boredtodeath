﻿Public Class Television
    Inherits Item

    Public Sub New()
        name = "Television"
        icon = My.Resources.TV
        isUsable = False
        dangerVal = 5
        funVal = 5
        description = vbCrLf & "Dude, the power went out.  The TV won't turn on." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You stare into the TV hoping for life to spring up from that blank, life-less miracle of creation.  But it doesn't..."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Unbeknownst to you, the cat jumps up behind the TV and pushes it on top of you.  Too much TV really CAN kill you."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
