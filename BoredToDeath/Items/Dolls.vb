﻿Public Class Dolls
    Inherits Item

    Public Sub New()
        name = "Dolls"
        icon = My.Resources.Dolls
        isUsable = False
        dangerVal = 12
        funVal = 15
        description = vbCrLf & "Creepy dolls that belong to that sister you don't actually have." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You have a staring contest with one of the dolls, you lose."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "You pull the string on the back of one of the dolls, you hear a faint raspy 'help me' and it steals your soul."
            Boredom = 100 'This makes you die, don't change this
            Status = 4
        End If

        Return result
    End Function
End Class
