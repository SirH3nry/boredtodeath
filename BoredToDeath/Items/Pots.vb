﻿Public Class Pots
    Inherits Item

    Public Sub New()
        name = "Pots&Pans"
        icon = My.Resources.Pots
        isUsable = False
        dangerVal = 1
        funVal = 25
        description = vbCrLf & "It's the set of pots and pans that your mom bought off of that infomercial." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You relive your dream as a pots and pans drummer like when you were a kid!"
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Your head, all of a sudden, becomes strongly magnetized and all of the pots and pans soar towards your skull."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class

