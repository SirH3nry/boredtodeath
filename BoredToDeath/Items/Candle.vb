﻿Public Class Candle
    Inherits Item

    Public Sub New()
        name = "Candle"
        icon = My.Resources.Candle
        isUsable = False
        dangerVal = 40
        funVal = 25
        description = vbCrLf & "42 home candle fires are reported every day." & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "You dip your fingers in the hot wax to entertain yourself."
            Boredom -= funVal
            If (Boredom < 0) Then
                Boredom = 0
            End If
        Else
            'If you're unlucky, this is displayed
            result = "Congratulations, you're a statistic, you burnt your house down."
            Boredom = 100 'This makes you die, don't change this
            Status = 1
        End If

        Return result
    End Function
End Class
