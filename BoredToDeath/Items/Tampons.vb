﻿Public Class Tampons
    Inherits Item

    Public Sub New()
        name = "Tampons"
        icon = My.Resources.Tampons
        isUsable = True
        dangerVal = 10
        funVal = 8
        description = vbCrLf & "5 inch diameter Industrial Strength Tampons" & " (Danger = " & dangerVal & ", Fun = " & funVal & ")"
    End Sub

    Public Overrides Function Use(ByRef Boredom As Integer, ByRef Status As UShort)
        Dim result As String = "" '<---DONT CHANGE THIS DECLARATION STATEMENT
        If dangerCalc(dangerVal) Then
            'If the item comes back safe, display this and lower the Boredom by this amount
            result = "The tampon proves to be a fun toy for a little while."
            Boredom -= funVal
        Else
            'If you're unlucky, this is displayed
            result = "While playing 'hide the tampon,' it gets stuck in an awkward orifice."
            Boredom = 100 'This makes you die, don't change this
        End If

        Return result
    End Function
End Class
