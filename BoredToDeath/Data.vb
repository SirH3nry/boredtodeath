﻿Public Class Data

    Public Shared FirstFloor(5, 5) As Room
    Public Shared SecondFloor(4, 5) As Room
    Public Shared Basement(2, 4) As Room

    'Public Enum Items
    '    BANDAIDS
    '    BASKETBALL
    '    BATTERIES
    '    BEER
    '    BLANKETS
    '    BLEACH
    '    BOX
    '    BULLETS
    '    CABINET
    '    CANDLES
    '    CAR
    '    CD
    '    CEILINGFAN
    '    DOLLS
    '    DOOR
    '    DRANO
    '    ENERGYDRINK
    '    FLASHLIGHT
    '    FOOD
    '    GUN
    '    HAMMER
    '    KNIFE
    '    LAMP
    '    LEGO
    '    LIGHTER
    '    MAGAZINE
    '    MATCHES
    '    PILLOWS
    '    POTS
    '    RATPOISON
    '    REFRIGERATOR
    '    ROLLERSKATES
    '    ROPE
    '    SHOVEL
    '    SINK
    '    SNOWMOBILE
    '    SNOWSHOVEL
    '    STAIRS
    '    STEREO
    '    STEVECAT
    '    STUFFEDANIMAL
    '    TAMPONS
    '    TELEVISION
    '    TOILET
    'End Enum

    Public Shared Sub FillMapData()
        Dim temp = New Room

        '+++++++++++Basement++++++++++++'
        '==============================='
        temp.Name = "Bathroom"
        temp.Description = "Walking into the bathroom, you see a neglected toilet with poo caked to the inside. Clearly no one uses it.  You also see a sink with a empty bottle of soap on it. The cabnet above the sink contains empty boxes of medicine except for the container of rat poison."
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New RatPoison)
        temp.ItemList.Add(New Drano)
        temp.ItemList.Add(New Sink)
        temp.ItemList.Add(New Bleach)
        temp.hasStairs = False
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = False

        Basement(1, 1) = temp
        temp = New Room
        '==============================='

        '===========Basement============'
        temp.Name = "Basement"
        temp.Description = "Upon going around the corner you see a moldy couch, a mini fridge, a few boxes, and an old tube TV."
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Television)
        temp.ItemList.Add(New Lamp)
        temp.ItemList.Add(New Beer)

        temp.hasStairs = False
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = False

        Basement(1, 2) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Basement"
        temp.Description = "In the corner you see a gun cabnet. You know your dad stashes all his guns inside. There is old sporting equipment in the opposite corner."
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New basketball)
        temp.ItemList.Add(New Bullets)
        temp.ItemList.Add(New Gun)
        temp.ItemList.Add(New RollerSkates)

        temp.hasStairs = False
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = False
        temp.exits(4) = True

        Basement(2, 1) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Basement"
        temp.Description = "There is a old warped bar near the wall of the basement.  It is covered with boxes, old papers, and trash."
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Magazine)
        temp.ItemList.Add(New Lamp)
        temp.hasStairs = False
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = True

        Basement(2, 2) = temp
        temp = New Room
        '==============================='

        '=======Hallway Basement========'
        temp.Name = "Hall"
        temp.Description = "Upon entering the basement, you see a lamp hanging from the ceiling with cobwebs covering it. There are old wooden stairs going to the upper floor and numerous boxes underneath them."
        temp.ItemList = New List(Of Item)
        temp.hasStairs = True
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = False

        Basement(1, 3) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Hall"
        temp.Description = "This room is mostly empty, there is a door to the main basement to the west and a door to the laundry room to the east."
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Lamp)

        temp.hasStairs = False
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = True

        Basement(2, 3) = temp
        temp = New Room
        '==============================='

        '==============================='
        temp.Name = "Storage"
        temp.Description = "Boxes, boxes everywhere."
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Rope)
        temp.ItemList.Add(New Knife)


        temp.hasStairs = False
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = False

        Basement(1, 4) = temp
        temp = New Room
        '==============================='

        '==============================='
        temp.Name = "Laundry"
        temp.Description = "There is that dryer smell that is stereotypical of laundries and a washer and dryer against the wall. Your mom must have been in the middle of drying towels when the power went out."
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Bleach)
        temp.ItemList.Add(New Blanket)

        temp.hasStairs = False
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = False

        Basement(2, 4) = temp
        temp = New Room
        '==============================='
        '+++++++++++++++++++++++++++++++'

        '+++++++++Second Floor++++++++++'
        '=========Your Bedroom=========='
        temp.Name = "Your Bedroom"
        temp.Description = "In this corner of your room, you see your unmade bed and night stand. On top of the nightstand is your alarm clock and a recharging DS. Too bad the batteries died right before the power went out(Insert sad trombone)."
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Pillows)
        temp.hasStairs = False
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = True

        SecondFloor(1, 4) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Your Bedroom"
        temp.Description = "On this side of the room is your desk and TV.  On the desk is your computer monitor which is connected to your computer. Connected to your TV is the brand new Steam console. "
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New EnergyDrink)
        temp.ItemList.Add(New Stereo)

        'temp.container = New Trunk(New Lego)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = False

        SecondFloor(1, 5) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Your Bedroom"
        temp.Description = "To your left is a book shelf full of books that you dont read and comic books that you do read. That's about it."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New CD)
        temp.ItemList.Add(New Food)
        temp.ItemList.Add(New CeilingFan)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = True

        SecondFloor(2, 4) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Your Bedroom"
        temp.Description = "In your closet is a bunch of clothes and boxes of things from when you were younger. There is also a box of you-know-what."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Blanket)

        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = False

        SecondFloor(2, 5) = temp
        temp = New Room
        '==============================='

        '=======Brother's Bedroom======='
        temp.Name = "Brother's Bedroom"
        temp.Description = "Your younger bothers room must have been recently cleaned up because all of the toys have been put away. Immedietely to your left is a chest full of toys. "
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New StuffedAnimal)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = True

        SecondFloor(3, 4) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Brother's Bedroom"
        temp.Description = "You see your brothers racecar bed in this corner along with his lego play table."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Lego)
        temp.ItemList.Add(New Blanket)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = False

        SecondFloor(3, 5) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Brother's Bedroom"
        temp.Description = "This corner of the room is just an open area for your brother to play in. There are some toys scattered about."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New StuffedAnimal)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = True

        SecondFloor(4, 4) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Brother's Bedroom"
        temp.Description = "Your brother's closet is filled with clothes and even more toys. How come you were never this loved?"
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Dolls)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = False

        SecondFloor(4, 5) = temp
        temp = New Room
        '==============================='

        '======Hallway Second Floor====='
        temp.Name = "Hall"
        temp.Description = "The upstairs hall way is pretty unremarkable. Just two linen closets and doors to other rooms."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Blanket)
        temp.ItemList.Add(New Bleach)
        temp.ItemList.Add(New Pillows)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = True

        SecondFloor(2, 3) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Hall"
        temp.Description = "As you move down the hall you see pictures of your family. You always hated family photos because you always thought you were different."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New CeilingFan)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = True

        SecondFloor(3, 3) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Hall"
        temp.Description = "Just more hallway. There is a closet here however which you have never opened before. I wonder what could be inside."
        temp.hasStairs = True
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Pots)
        temp.ItemList.Add(New Knife)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = False
        temp.exits(4) = False

        SecondFloor(4, 3) = temp
        temp = New Room
        '==============================='

        '=======Parents' Bedroom========'
        temp.Name = "Parents' Bedroom"
        temp.Description = "There is a photo here of your parents wedding. So corny."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Dolls)
        temp.ItemList.Add(New CeilingFan)
        temp.ItemList.Add(New Rope)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = True

        SecondFloor(2, 1) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Parents' Bedroom"
        temp.Description = "You find your parents trunk in this corner and above that is picture of your parents. Your family sure does love pictures."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New CD)
        temp.ItemList.Add(New Batteries)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = False

        SecondFloor(2, 2) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Parents' Bedroom"
        temp.Description = "Here is your parents' bed. You don't dare to think on it tough; thinking brings illness."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Candle)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = False

        SecondFloor(3, 1) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Parents' Bedroom"
        temp.Description = "Even entering your Parents' bedroom gives you the chills. You don't know what goes on in here and you dont want to know."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Blanket)
        temp.ItemList.Add(New Pillows)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = True

        SecondFloor(3, 2) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Parents' Bedroom"
        temp.Description = "In this corner of the room is your mom's jazzercize equipment and some weights."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Stereo)
        temp.ItemList.Add(New BandAids)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = False
        temp.exits(4) = True

        SecondFloor(4, 1) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Parents' Bedroom"
        temp.Description = "Here is your parents' TV.  Who knows what they use this for, but it can't be good."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Television)

        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = False

        SecondFloor(4, 2) = temp
        temp = New Room
        '==============================='

        '==============================='
        temp.Name = "Parents' Bathroom"
        temp.Description = "Upon entering your parents' bathroom, you can't find a trace of masculinity. Feminine products everywhere. Godspeed."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Tampons)
        temp.ItemList.Add(New BandAids)
        temp.ItemList.Add(New Candle)
        temp.ItemList.Add(New Sink)

        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = False

        SecondFloor(1, 1) = temp
        temp = New Room
        '==============================='

        '==============================='
        temp.Name = "Parents' Closet"
        temp.Description = "You open the door to your parents' closet and junk pours out, you're pretty sure you saw something moving in there as well."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New SteveCat)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = False

        SecondFloor(1, 2) = temp
        temp = New Room
        '==============================='
        '+++++++++++++++++++++++++++++++'

        '++++++++++First Floor++++++++++'
        '============Kitchen============'
        temp.Name = "Kitchen"
        temp.Description = "Here is the refigerator and a few cabnets. It's a shame the power is out, Hot Pockets sound awesome right now."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New EnergyDrink)
        temp.ItemList.Add(New Beer)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = True

        FirstFloor(1, 1) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Kitchen"
        temp.Description = "You come across the stove. Ever since you burned your hand on it when you were little you have been scared of it."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Pots)
        temp.ItemList.Add(New CeilingFan)
        temp.ItemList.Add(New Lighter)

        'assign exits
        temp.exits(1) = False
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = False

        FirstFloor(1, 2) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Kitchen"
        temp.Description = "Welcome to the pantry. It is filled with food from floor to ceiling."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Food)
        temp.ItemList.Add(New Matches)
        temp.ItemList.Add(New Blanket)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = False
        temp.exits(4) = True

        FirstFloor(2, 1) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Kitchen"
        temp.Description = "Here is the counter that is strangely empty. Usually your mom has recipe books and inredients everywhere."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Knife)
        temp.ItemList.Add(New Sink)
        temp.ItemList.Add(New Drano)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = True

        FirstFloor(2, 2) = temp
        temp = New Room
        '==============================='

        '==========Dining Room=========='
        temp.Name = "Dining Room"
        temp.Description = "On this side of the room is an end of the dining room table and a china cabinet filled with the nice dishes that your mom breaks out during holidays."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Matches)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = True

        FirstFloor(3, 1) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Dining Room"
        temp.Description = "This side of the dining room as the other end of the table and also another family photo. The eyes of the photo are following you. creepy."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New CeilingFan)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = False

        FirstFloor(3, 2) = temp
        temp = New Room
        '==============================='

        '==========Living Room=========='
        temp.Name = "Living Room"
        temp.Description = "This side of the living room contains a table and some chairs. You can sit and watch TV here. The best combo ever."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Television)
        temp.ItemList.Add(New CD)
        temp.ItemList.Add(New Candle)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = True

        FirstFloor(4, 1) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Living Room"
        temp.Description = "Against the wall is a couch covered in some ugly floral pattern. It's still comfy though so you wont hold its horrible sense of style against it."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New CeilingFan)
        temp.ItemList.Add(New Blanket)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = True

        FirstFloor(4, 2) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Living Room"
        temp.Description = "In this corner is a desk area for your parents. Here they fail using the computer daily."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Batteries)
        temp.ItemList.Add(New Knife)
        temp.ItemList.Add(New CD)

        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = False
        temp.exits(4) = True

        FirstFloor(5, 1) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Living Room"
        temp.Description = "Moving into the next area, you see a TV that is rarely used."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Television)
        temp.ItemList.Add(New Stereo)

        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = False

        FirstFloor(5, 2) = temp
        temp = New Room
        '==============================='

        '==============================='
        temp.Name = "Bathroom"
        temp.Description = "Walking into the most cliche hall bath ever you see sea shells on the sink and horrible wallpaper."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New BandAids)
        temp.ItemList.Add(New Sink)

        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = False

        FirstFloor(1, 3) = temp
        temp = New Room
        '==============================='

        '======Hallway First Floor======'
        temp.Name = "Hall"
        temp.Description = "At the end of the hall is a bunch of doors. Which one has the car and which are goats. Just kidding they lead to more rooms."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = True

        FirstFloor(2, 3) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Hall"
        temp.Description = "Moving forward you there are picture on one wall and little table with knick-knacks against the other."
        temp.hasStairs = True
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Candle)
        temp.ItemList.Add(New Magazine)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = False

        FirstFloor(3, 3) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Hall"
        temp.Description = "A fairly unremarkable hallway that connects most of the rooms on the first floor."
        temp.hasStairs = True
        temp.ItemList = New List(Of Item)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = False

        FirstFloor(4, 3) = temp
        temp = New Room
        '==============================='

        '==============================='
        temp.Name = "Foyer"
        temp.Description = "The foyer is the welcome area and has stairs leading to the second floor."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New RollerSkates)
        temp.ItemList.Add(New CeilingFan)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = False
        temp.exits(4) = True

        FirstFloor(5, 3) = temp
        temp = New Room
        '==============================='

        '===========Game Room==========='
        temp.Name = "Game Room"
        temp.Description = "In this corner of the game room is the pool table. You try and beat your dad every now and then but your dad went semi-pro in billiards so you never had a chance."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Lamp)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = True

        FirstFloor(1, 4) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Game Room"
        temp.Description = "Welcome the the game room. It is full of things to do and the best room in the house."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New CeilingFan)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = True

        FirstFloor(2, 4) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Game Room"
        temp.Description = "Against the wall is the largest TV screen in history. The TV is literally the wall."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Television)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = False
        temp.exits(3) = False
        temp.exits(4) = True

        FirstFloor(3, 4) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Game Room"
        temp.Description = "In this corner is where the air hockey table is. You are currently house champion and hold that title proudly."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Batteries)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = False

        FirstFloor(1, 5) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Game Room"
        temp.Description = "Against the wall is the largest and most comfy couch in existence. You sit here and watch movies all the time."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Lamp)
        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = False

        FirstFloor(2, 5) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Game Room"
        temp.Description = "Against the wall is the other half of the wall TV. If only the power was on and you could watch it."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Television)

        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = False

        FirstFloor(3, 5) = temp
        temp = New Room
        '==============================='

        '============Garage============='
        temp.Name = "Garage"
        temp.Description = "Moving further into the garage you stumble across your dad's tool bench. He fixes some things from time to time but otherwise it doesn't see any use."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Hammer)
        temp.ItemList.Add(New Chainsaw)
        'assign exits
        temp.exits(1) = False
        temp.exits(2) = False
        temp.exits(3) = True
        temp.exits(4) = True

        FirstFloor(4, 4) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Garage"
        temp.Description = "This area has more equipment like the lawn mower and other larger tools."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Shovel)
        temp.ItemList.Add(New Rope)
        temp.ItemList.Add(New Lighter)

        'assign exits
        temp.exits(1) = False
        temp.exits(2) = True
        temp.exits(3) = True
        temp.exits(4) = False

        FirstFloor(4, 5) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Garage"
        temp.Description = "The garage is dark and smelly. Immedietely to your left is the laundry machine and dryer."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Bleach)
        temp.ItemList.Add(New Drano)
        temp.ItemList.Add(New RatPoison)

        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = True

        FirstFloor(5, 4) = temp
        temp = New Room
        '-------------------------------'
        temp.Name = "Garage"
        temp.Description = "There sits your dad's prized possession - a red Ferrari Enzo."
        temp.hasStairs = False
        temp.ItemList = New List(Of Item)
        temp.ItemList.Add(New Lighter)
        temp.ItemList.Add(New Batteries)

        'assign exits
        temp.exits(1) = True
        temp.exits(2) = True
        temp.exits(3) = False
        temp.exits(4) = False

        FirstFloor(5, 5) = temp
        '==============================='
        '+++++++++++++++++++++++++++++++'

    End Sub
End Class
