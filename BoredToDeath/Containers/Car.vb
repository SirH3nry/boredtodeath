﻿Public Class Car
    Inherits OpenObject

    Public Sub New(ByVal i As Item)
        name = "Car"
        description = "A very exquisite Ferrari Enzo."
        contains = i
        openMessage = "Upon opening the car you discover that a " & contains.name & " is inside."
    End Sub

End Class
