﻿Public Class Cabinet
    Inherits OpenObject

    Public Sub New(ByVal i As Item)
        name = "Cabinet"
        description = "A fairly typical wooden cabinet.  There is no telling what's inside."
        contains = i
        openMessage = "Upon opening the cabinet you discover that a " & contains.name & " is inside."
    End Sub

End Class
