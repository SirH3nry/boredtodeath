﻿Public Class Toilet
    Inherits OpenObject

    Public Sub New(ByVal i As Item)
        name = "Toilet"
        description = "A typical porcelain toliet, generally used to dispose human waste."
        contains = i
        openMessage = "Upon opening the toilet you discover that a " & contains.name & " is inside."
    End Sub

End Class
