﻿Public Class Trunk
    Inherits OpenObject

    Public Sub New(ByVal i As Item)
        name = "Trunk"
        description = "A handcrafted wooden trunk with metal latches."
        contains = i
        openMessage = "Upon opening the trunk you discover that a " & contains.name & " is inside."
    End Sub

End Class
