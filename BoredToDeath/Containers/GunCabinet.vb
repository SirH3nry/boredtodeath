﻿Public Class GunCabinet
    Inherits OpenObject

    Public Sub New(ByVal i As Item)
        name = "GunCabinet"
        description = "A tall wooden cabinet with a glass door, you can see a sinlge rifle resting inside."
        contains = i
        openMessage = "Upon opening the cabinet you discover that a " & contains.name & " is inside."
    End Sub

End Class
