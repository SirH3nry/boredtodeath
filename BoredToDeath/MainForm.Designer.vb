﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MainForm
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.txtInput = New System.Windows.Forms.TextBox()
        Me.lblBoredom = New System.Windows.Forms.Label()
        Me.IntroTimer = New System.Windows.Forms.Timer(Me.components)
        Me.btnSubmit = New System.Windows.Forms.Button()
        Me.BoredomTimer = New System.Windows.Forms.Timer(Me.components)
        Me.WinTimer = New System.Windows.Forms.Timer(Me.components)
        Me.picFlashlight = New System.Windows.Forms.PictureBox()
        Me.picInventory9 = New System.Windows.Forms.PictureBox()
        Me.picInventory7 = New System.Windows.Forms.PictureBox()
        Me.picInventory5 = New System.Windows.Forms.PictureBox()
        Me.picInventory3 = New System.Windows.Forms.PictureBox()
        Me.picInventory1 = New System.Windows.Forms.PictureBox()
        Me.picInventory8 = New System.Windows.Forms.PictureBox()
        Me.picInventory6 = New System.Windows.Forms.PictureBox()
        Me.picInventory4 = New System.Windows.Forms.PictureBox()
        Me.picInventory2 = New System.Windows.Forms.PictureBox()
        Me.picInventory0 = New System.Windows.Forms.PictureBox()
        Me.lblOutput = New System.Windows.Forms.Label()
        CType(Me.picFlashlight, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picInventory9, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picInventory7, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picInventory5, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picInventory3, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picInventory1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picInventory8, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picInventory6, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picInventory4, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picInventory2, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.picInventory0, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'txtInput
        '
        Me.txtInput.Location = New System.Drawing.Point(12, 398)
        Me.txtInput.Name = "txtInput"
        Me.txtInput.Size = New System.Drawing.Size(512, 20)
        Me.txtInput.TabIndex = 2
        '
        'lblBoredom
        '
        Me.lblBoredom.BackColor = System.Drawing.Color.Transparent
        Me.lblBoredom.Location = New System.Drawing.Point(530, 356)
        Me.lblBoredom.Name = "lblBoredom"
        Me.lblBoredom.Size = New System.Drawing.Size(135, 35)
        Me.lblBoredom.TabIndex = 111
        Me.lblBoredom.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'IntroTimer
        '
        '
        'btnSubmit
        '
        Me.btnSubmit.BackColor = System.Drawing.Color.Black
        Me.btnSubmit.ForeColor = System.Drawing.Color.White
        Me.btnSubmit.Location = New System.Drawing.Point(530, 398)
        Me.btnSubmit.Name = "btnSubmit"
        Me.btnSubmit.Size = New System.Drawing.Size(75, 23)
        Me.btnSubmit.TabIndex = 1
        Me.btnSubmit.Text = "Submit"
        Me.btnSubmit.UseVisualStyleBackColor = False
        '
        'BoredomTimer
        '
        Me.BoredomTimer.Interval = 1000
        '
        'WinTimer
        '
        Me.WinTimer.Enabled = True
        Me.WinTimer.Interval = 500000
        '
        'picFlashlight
        '
        Me.picFlashlight.BackColor = System.Drawing.Color.Transparent
        Me.picFlashlight.BackgroundImage = Global.BoredToDeath.My.Resources.Resources.FlashlightOff
        Me.picFlashlight.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch
        Me.picFlashlight.Location = New System.Drawing.Point(633, 394)
        Me.picFlashlight.Name = "picFlashlight"
        Me.picFlashlight.Size = New System.Drawing.Size(32, 32)
        Me.picFlashlight.TabIndex = 124
        Me.picFlashlight.TabStop = False
        Me.picFlashlight.Visible = False
        '
        'picInventory9
        '
        Me.picInventory9.BackColor = System.Drawing.Color.Transparent
        Me.picInventory9.Location = New System.Drawing.Point(601, 289)
        Me.picInventory9.Name = "picInventory9"
        Me.picInventory9.Size = New System.Drawing.Size(64, 64)
        Me.picInventory9.TabIndex = 10
        Me.picInventory9.TabStop = False
        '
        'picInventory7
        '
        Me.picInventory7.BackColor = System.Drawing.Color.Transparent
        Me.picInventory7.Location = New System.Drawing.Point(601, 219)
        Me.picInventory7.Name = "picInventory7"
        Me.picInventory7.Size = New System.Drawing.Size(64, 64)
        Me.picInventory7.TabIndex = 9
        Me.picInventory7.TabStop = False
        '
        'picInventory5
        '
        Me.picInventory5.BackColor = System.Drawing.Color.Transparent
        Me.picInventory5.Location = New System.Drawing.Point(601, 149)
        Me.picInventory5.Name = "picInventory5"
        Me.picInventory5.Size = New System.Drawing.Size(64, 64)
        Me.picInventory5.TabIndex = 8
        Me.picInventory5.TabStop = False
        '
        'picInventory3
        '
        Me.picInventory3.BackColor = System.Drawing.Color.Transparent
        Me.picInventory3.Location = New System.Drawing.Point(601, 79)
        Me.picInventory3.Name = "picInventory3"
        Me.picInventory3.Size = New System.Drawing.Size(64, 64)
        Me.picInventory3.TabIndex = 7
        Me.picInventory3.TabStop = False
        '
        'picInventory1
        '
        Me.picInventory1.BackColor = System.Drawing.Color.Transparent
        Me.picInventory1.Location = New System.Drawing.Point(601, 9)
        Me.picInventory1.Name = "picInventory1"
        Me.picInventory1.Size = New System.Drawing.Size(64, 64)
        Me.picInventory1.TabIndex = 7
        Me.picInventory1.TabStop = False
        '
        'picInventory8
        '
        Me.picInventory8.BackColor = System.Drawing.Color.Transparent
        Me.picInventory8.Location = New System.Drawing.Point(530, 289)
        Me.picInventory8.Name = "picInventory8"
        Me.picInventory8.Size = New System.Drawing.Size(64, 64)
        Me.picInventory8.TabIndex = 6
        Me.picInventory8.TabStop = False
        '
        'picInventory6
        '
        Me.picInventory6.BackColor = System.Drawing.Color.Transparent
        Me.picInventory6.Location = New System.Drawing.Point(530, 219)
        Me.picInventory6.Name = "picInventory6"
        Me.picInventory6.Size = New System.Drawing.Size(64, 64)
        Me.picInventory6.TabIndex = 5
        Me.picInventory6.TabStop = False
        '
        'picInventory4
        '
        Me.picInventory4.BackColor = System.Drawing.Color.Transparent
        Me.picInventory4.Location = New System.Drawing.Point(530, 149)
        Me.picInventory4.Name = "picInventory4"
        Me.picInventory4.Size = New System.Drawing.Size(64, 64)
        Me.picInventory4.TabIndex = 4
        Me.picInventory4.TabStop = False
        '
        'picInventory2
        '
        Me.picInventory2.BackColor = System.Drawing.Color.Transparent
        Me.picInventory2.Location = New System.Drawing.Point(530, 79)
        Me.picInventory2.Name = "picInventory2"
        Me.picInventory2.Size = New System.Drawing.Size(64, 64)
        Me.picInventory2.TabIndex = 3
        Me.picInventory2.TabStop = False
        '
        'picInventory0
        '
        Me.picInventory0.BackColor = System.Drawing.Color.Transparent
        Me.picInventory0.Location = New System.Drawing.Point(530, 9)
        Me.picInventory0.Name = "picInventory0"
        Me.picInventory0.Size = New System.Drawing.Size(64, 64)
        Me.picInventory0.TabIndex = 2
        Me.picInventory0.TabStop = False
        '
        'lblOutput
        '
        Me.lblOutput.BackColor = System.Drawing.Color.Black
        Me.lblOutput.ForeColor = System.Drawing.Color.White
        Me.lblOutput.Location = New System.Drawing.Point(12, 9)
        Me.lblOutput.Name = "lblOutput"
        Me.lblOutput.Size = New System.Drawing.Size(512, 386)
        Me.lblOutput.TabIndex = 123
        '
        'MainForm
        '
        Me.AcceptButton = Me.btnSubmit
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.Gray
        Me.ClientSize = New System.Drawing.Size(688, 431)
        Me.Controls.Add(Me.picFlashlight)
        Me.Controls.Add(Me.btnSubmit)
        Me.Controls.Add(Me.lblBoredom)
        Me.Controls.Add(Me.picInventory9)
        Me.Controls.Add(Me.picInventory7)
        Me.Controls.Add(Me.picInventory5)
        Me.Controls.Add(Me.picInventory3)
        Me.Controls.Add(Me.picInventory1)
        Me.Controls.Add(Me.picInventory8)
        Me.Controls.Add(Me.picInventory6)
        Me.Controls.Add(Me.picInventory4)
        Me.Controls.Add(Me.picInventory2)
        Me.Controls.Add(Me.picInventory0)
        Me.Controls.Add(Me.lblOutput)
        Me.Controls.Add(Me.txtInput)
        Me.Name = "MainForm"
        Me.Text = "Bored to Death"
        CType(Me.picFlashlight, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picInventory9, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picInventory7, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picInventory5, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picInventory3, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picInventory1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picInventory8, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picInventory6, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picInventory4, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picInventory2, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.picInventory0, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents txtInput As System.Windows.Forms.TextBox
    Friend WithEvents picInventory0 As System.Windows.Forms.PictureBox
    Friend WithEvents picInventory2 As System.Windows.Forms.PictureBox
    Friend WithEvents picInventory4 As System.Windows.Forms.PictureBox
    Friend WithEvents picInventory6 As System.Windows.Forms.PictureBox
    Friend WithEvents picInventory8 As System.Windows.Forms.PictureBox
    Friend WithEvents picInventory1 As System.Windows.Forms.PictureBox
    Friend WithEvents picInventory3 As System.Windows.Forms.PictureBox
    Friend WithEvents picInventory5 As System.Windows.Forms.PictureBox
    Friend WithEvents picInventory7 As System.Windows.Forms.PictureBox
    Friend WithEvents picInventory9 As System.Windows.Forms.PictureBox
    Friend WithEvents lblBoredom As System.Windows.Forms.Label
    Friend WithEvents IntroTimer As System.Windows.Forms.Timer
    Friend WithEvents btnSubmit As System.Windows.Forms.Button
    Friend WithEvents BoredomTimer As System.Windows.Forms.Timer
    Friend WithEvents picFlashlight As System.Windows.Forms.PictureBox
    Friend WithEvents WinTimer As System.Windows.Forms.Timer
    Friend WithEvents lblOutput As System.Windows.Forms.Label

End Class
