﻿Public MustInherit Class OpenObject
    Public name As String
    Public description As String
    Public openMessage As String
    Public contains As Item
    Public opened As Boolean = False
End Class