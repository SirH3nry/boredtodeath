﻿Public Class PlayerPosition
    Private r, c, f As Integer
    Property Row() As Integer
        Get
            Return r
        End Get
        Set(ByVal value As Integer)
            r = value
        End Set
    End Property
    Property Column() As Integer
        Get
            Return c
        End Get
        Set(ByVal value As Integer)
            c = value
        End Set
    End Property
    Property Floor() As Integer
        Get
            Return f
        End Get
        Set(ByVal value As Integer)
            f = value
        End Set
    End Property
End Class
