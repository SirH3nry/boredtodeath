﻿Public Class MainForm
    Public IntroCount As Integer = 0
    Public Command As String
    Public BoredomPercent As Decimal
    Public Status As UShort = 0
    Public Shared Position As PlayerPosition = New PlayerPosition()
    Public Shared Inventory As List(Of Item) = New List(Of Item)
    Public Shared ReadOnly picList(9) As PictureBox
    Public curRoom As Room
    Public start As Boolean = True  'boolean flag to determine if the game started
    Public hasLight As Boolean = False
    Public scr As Integer = 0
    Public help As Integer = 0
    Public WIN As Boolean = False

    Public Enum CommandList
        USE
        TAKE
        EXAMINE
        SEARCH
        MOVE
        WALK
        GO
        DROP
        OPEN
        SUICIDE
        STAIRS
        SORT
        HELP
        CREDITS
        MASTURBATE
    End Enum

    Public Enum Direction
        NORTH
        UP
        SOUTH
        DOWN
        WEST
        LEFT
        EAST
        RIGHT
    End Enum

    Private Sub MainForm_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        IntroTimer.Enabled = True
        IntroTimer.Start()
        Data.FillMapData()
        Position.Row = 1
        Position.Column = 5
        Position.Floor = 2
        curRoom = getCurRoom()
        lblOutput.BackColor = Color.White
        lblOutput.ForeColor = Color.Black
        lblOutput.Text = "You are in the middle of browsing reddit..."
        btnSubmit.Visible = False
        txtInput.Visible = False
        picList(0) = picInventory0
        picList(1) = picInventory1
        picList(2) = picInventory2
        picList(3) = picInventory3
        picList(4) = picInventory4
        picList(5) = picInventory5
        picList(6) = picInventory6
        picList(7) = picInventory7
        picList(8) = picInventory8
        picList(9) = picInventory9
    End Sub

    Private Sub Intro_Tick(sender As System.Object, e As System.EventArgs) Handles IntroTimer.Tick
        IntroCount = IntroCount + 1
        If (IntroCount = 10) Then
            lblOutput.Text &= vbCrLf & "..."
        End If
        If (IntroCount = 20) Then
            lblOutput.Text &= vbCrLf & "..."
        End If
        If (IntroCount = 30) Then
            lblOutput.Text &= vbCrLf & "...when the power goes out."
            lblOutput.Text &= vbCrLf & "You wait for your eyes to adjust, but it is difficult to see far in any direction..." & vbCrLf & "What do?"
            lblOutput.BackColor = Color.Black
            lblOutput.ForeColor = Color.White
            btnSubmit.Visible = True
            txtInput.Visible = True
            IntroTimer.Dispose()
            BoredomTimer.Enabled = True
            BoredomTimer.Start()
            WinTimer.Start()
            lblBoredom.Image = My.Resources.BoredomBar_0
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If (txtInput.Text <> "") Then
            Dim Instruction = txtInput.Text.ToUpper()
            Dim i As Integer
            Dim j As Integer
            i = Instruction.IndexOf(" ")
            If (i < 0) Then
                Command = Instruction
                If (Command.Equals(CommandList.WALK.ToString)) Then
                    lblOutput.Text &= vbCrLf & "Walk where?"
                ElseIf (Command.Equals(CommandList.MOVE.ToString)) Then
                    lblOutput.Text &= vbCrLf & "Move where?"
                ElseIf (Command.Equals(CommandList.GO.ToString)) Then
                    lblOutput.Text &= vbCrLf & "Go where?"
                ElseIf (Command.Equals(CommandList.USE.ToString)) Then
                    lblOutput.Text &= vbCrLf & "Use what?"
                ElseIf (Command.Equals(CommandList.TAKE.ToString)) Then
                    If ((hasLight = False) And (picFlashlight.Visible)) Then
                        lblOutput.Text &= vbCrLf & "You crawl along the ground searching for what you tripped over. It ends up being a flashlight, how convenient."
                        hasLight = True
                        picFlashlight.BackgroundImage = My.Resources.FlashlightOn
                    Else
                        lblOutput.Text &= vbCrLf & "Take what?"
                    End If
                ElseIf (Command.Equals(CommandList.SEARCH.ToString)) Then
                    If (hasLight = False) Then
                        If (picFlashlight.Visible) Then
                            lblOutput.Text &= vbCrLf & "You crawl along the ground searching for what you tripped over. It ends up being a flashlight, how convenient."
                            hasLight = True
                            picFlashlight.BackgroundImage = My.Resources.FlashlightOn
                        Else
                            lblOutput.Text &= vbCrLf & "You notice nothing, because there is no light, and you are unable to see in the dark."
                        End If
                    Else
                        lblOutput.Text &= vbCrLf & "You notice these items in the room: "
                        For Each it As Item In curRoom.ItemList
                            lblOutput.Text &= vbCrLf & it.name
                        Next
                        If (curRoom.container IsNot Nothing) Then
                            If (curRoom.container.opened = True) Then
                                lblOutput.Text &= vbCrLf & curRoom.container.contains.name
                            End If
                        End If
                    End If
                ElseIf (Command.Equals(CommandList.EXAMINE.ToString)) Then
                    lblOutput.Text &= vbCrLf & "Examine what?"
                ElseIf (Command.Equals(CommandList.DROP.ToString)) Then
                    lblOutput.Text &= vbCrLf & "Drop what?"
                ElseIf (Command.Equals(CommandList.OPEN.ToString)) Then
                    lblOutput.Text &= vbCrLf & "Open what?"
                ElseIf (Command.Equals(CommandList.CREDITS.ToString)) Then
                    lblOutput.Text &= vbCrLf & "Created by: Shane Shafferman, Alex Yochim, Steve Chalker, Peter Kalmar, and Martin Seibert."
                ElseIf (Command.Equals(CommandList.STAIRS.ToString)) Then
                    If (isStairsValid()) Then
                        enterNewFloor()
                    Else
                        lblOutput.Text &= vbCrLf & "There are no stairs here."
                    End If
                ElseIf (Command.Equals(CommandList.SORT.ToString)) Then
                    Inventory.Sort(Function(i1, i2) i1.name.CompareTo(i2.name))
                    lblOutput.Text &= vbCrLf & "As you enter the command, the computer performs a rudimentary bubble sort on your items.  Your inventory now looks as follows: "
                    For Each it As Item In Inventory
                        lblOutput.Text &= it.name & " "
                    Next
                    For index As Integer = 0 To Inventory.Count - 1
                        picList(index).Image = Inventory(index).icon
                    Next
                    picList(Inventory.Count).Image = Nothing
                ElseIf (Command.Equals(CommandList.SUICIDE.ToString)) Then
                    BoredomPercent = 100
                ElseIf (Command.Equals(CommandList.HELP.ToString)) Then
                    lblOutput.Text &= vbCrLf & "Its a text adventure, you figure it out."

                Else ' Handles Unknown Commands
                    lblOutput.Text &= vbCrLf & "I don't understand the """ & txtInput.Text & """ command..."
                End If
            Else
                Command = Instruction.Substring(0, i)
                j = Instruction.IndexOf(" ", i + 1)
                Dim Target As String

                If (j < 0) Then
                    Target = Instruction.Substring(i + 1)
                Else
                    Target = Instruction.Substring(i + 1, j - i)
                End If

                If (Command.Equals(CommandList.WALK.ToString) Or Command.Equals(CommandList.MOVE.ToString) Or Command.Equals(CommandList.GO.ToString)) Then
                    If ((Target.Equals(Direction.NORTH.ToString)) Or (Target.Equals(Direction.UP.ToString))) Then
                        If (isDirectionValid(1)) Then
                            enterNewRoom(1)
                        Else
                            If (hasLight = False) Then
                                lblOutput.Text &= vbCrLf & "You start to move in that direction, and walk straight into a wall..."
                            End If
                        End If
                    ElseIf ((Target.Equals(Direction.SOUTH.ToString)) Or (Target.Equals(Direction.DOWN.ToString))) Then
                        If (isDirectionValid(3)) Then
                            enterNewRoom(3)
                        Else
                            If (hasLight = False) Then
                                lblOutput.Text &= vbCrLf & "You start to move in that direction, and walk straight into a wall..."
                            End If
                        End If
                    ElseIf ((Target.Equals(Direction.WEST.ToString)) Or (Target.Equals(Direction.LEFT.ToString))) Then
                        If (isDirectionValid(2)) Then
                            enterNewRoom(2)
                        Else
                            If (hasLight = False) Then
                                lblOutput.Text &= vbCrLf & "You start to move in that direction, and walk straight into a wall..."
                            End If
                        End If
                    ElseIf ((Target.Equals(Direction.EAST.ToString)) Or (Target.Equals(Direction.RIGHT.ToString))) Then
                        If (isDirectionValid(4)) Then
                            enterNewRoom(4)
                        Else
                            If (hasLight = False) Then
                                lblOutput.Text &= vbCrLf & "You start to move in that direction, and walk straight into a wall..."
                            End If
                        End If
                    End If
                ElseIf (Command.Equals(CommandList.USE.ToString)) Then
                    Dim dne As Boolean = True
                    For Each it As Item In Inventory
                        If (Target.Equals(it.name.ToUpper())) Then
                            lblOutput.Text &= vbCrLf & it.Use(BoredomPercent, Status)
                            Inventory.Remove(it)
                            Inventory.Sort(Function(i1, i2) i1.name.CompareTo(i2.name))
                            For Each pb As PictureBox In picList
                                If (pb.Image Is it.icon) Then
                                    pb.Image = Nothing
                                    Exit For
                                End If
                            Next
                            For index As Integer = 0 To Inventory.Count - 1
                                picList(index).Image = Inventory(index).icon
                            Next
                            picList(Inventory.Count).Image = Nothing
                            dne = False
                            Exit For
                        End If
                    Next
                    If dne Then
                        lblOutput.Text &= vbCrLf & "You are not carrying that item..."
                    End If
                ElseIf (Command.Equals(CommandList.TAKE.ToString)) Then
                    Dim dne As Boolean = True
                    If hasLight Then
                        If (Inventory.Count < 10) Then
                            For Each it As Item In curRoom.ItemList
                                If (Target.Equals(it.name.ToUpper())) Then
                                    Inventory.Add(it)
                                    lblOutput.Text &= vbCrLf & "You acquired " & it.name
                                    For Each pb As PictureBox In picList
                                        If (pb.Image Is Nothing) Then
                                            pb.Image = it.icon
                                            Exit For
                                        End If
                                    Next
                                    dne = False
                                    curRoom.ItemList.Remove(it)
                                    Exit For
                                End If
                            Next
                            If (curRoom.container IsNot Nothing) Then
                                If (curRoom.container.opened = True) Then
                                    If (Target.Equals(curRoom.container.contains.name.ToUpper())) Then
                                        Inventory.Add(curRoom.container.contains)
                                        lblOutput.Text &= vbCrLf & "You acquired " & curRoom.container.contains.name
                                        For Each pb As PictureBox In picList
                                            If (pb.Image Is Nothing) Then
                                                pb.Image = curRoom.container.contains.icon
                                                Exit For
                                            End If
                                        Next
                                        dne = False
                                        curRoom.container = Nothing
                                    End If
                                End If
                            End If
                            If dne Then
                                lblOutput.Text &= vbCrLf & "You don't see that item..."
                            End If
                        Else
                            lblOutput.Text &= vbCrLf & "You can't carry any more items..."
                        End If
                    End If
                ElseIf (Command.Equals(CommandList.EXAMINE.ToString)) Then
                    Dim dne As Boolean = True
                    Dim noc As Boolean = True
                    If (hasLight) Then
                        For Each it As Item In Inventory
                            If (Target.Equals(it.name.ToUpper())) Then
                                lblOutput.Text &= vbCrLf & it.description
                                dne = False
                            End If
                        Next
                        If (curRoom.container IsNot Nothing) Then
                            If (Target.Equals(curRoom.container.name.ToUpper())) Then
                                lblOutput.Text &= vbCrLf & curRoom.container.description
                                noc = False
                            End If
                        End If
                        If dne And noc Then
                            lblOutput.Text &= vbCrLf & "You are not carrying that item..."
                        End If
                    End If
                ElseIf (Command.Equals(CommandList.SEARCH.ToString)) Then
                    If (hasLight = False) Then
                        If (picFlashlight.Visible) Then
                            lblOutput.Text &= vbCrLf & "You crawl along the ground searching for what you tripped over. It ends up being a flashlight, how convenient."
                            hasLight = True
                            picFlashlight.BackgroundImage = My.Resources.FlashlightOn
                        Else
                            lblOutput.Text &= vbCrLf & "You notice nothing, because there is no light, and you are unable to see in the dark."
                        End If
                    Else
                        lblOutput.Text &= vbCrLf & "You notice these items in the room: "
                        For Each it As Item In curRoom.ItemList
                            lblOutput.Text &= vbCrLf & it.name
                        Next
                        If (curRoom.container IsNot Nothing) Then
                            If (curRoom.container.opened = True) Then
                                lblOutput.Text &= vbCrLf & curRoom.container.contains.name
                            End If
                        End If
                    End If
                ElseIf (Command.Equals(CommandList.DROP.ToString)) Then
                    Dim dne As Boolean = True
                    For Each it As Item In Inventory
                        If (Target.Equals(it.name.ToUpper())) Then
                            lblOutput.Text &= vbCrLf & "You dropped the " & it.name & ".  It was never seen again..."
                            Inventory.Remove(it)
                            Inventory.Sort(Function(i1, i2) i1.name.CompareTo(i2.name))
                            For index As Integer = 0 To Inventory.Count - 1
                                picList(index).Image = Inventory(index).icon
                            Next
                            picList(Inventory.Count).Image = Nothing
                            dne = False
                            Exit For
                        End If
                    Next
                    If dne Then
                        lblOutput.Text &= vbCrLf & "You are not carrying that item..."
                    End If
                ElseIf (Command.Equals(CommandList.OPEN.ToString)) Then
                    If (hasLight) Then
                        If (curRoom.container IsNot Nothing) Then
                            If (Target.Equals(curRoom.container.name.ToUpper())) Then
                                curRoom.container.opened = True
                                lblOutput.Text &= vbCrLf & curRoom.container.openMessage
                            Else
                                lblOutput.Text &= vbCrLf & "You don't see a " & Target & " here."
                            End If
                        Else
                            lblOutput.Text &= vbCrLf & "There is nothing to open here."
                        End If
                    ElseIf (Command.Equals(CommandList.STAIRS.ToString)) Then
                        If (isStairsValid()) Then
                            enterNewFloor()
                        Else
                            lblOutput.Text &= vbCrLf & "There are no stairs here."
                        End If
                    Else ' Handles Unknown Commands
                        lblOutput.Text &= vbCrLf & "I don't understand the """ & txtInput.Text.Substring(0, i) & """ command..."
                    End If
                End If
            End If
            txtInput.Text = ""
        End If
    End Sub

    Private Sub Boredom_Tick(sender As System.Object, e As System.EventArgs) Handles BoredomTimer.Tick
        If (BoredomPercent <> 100.0) Then
            BoredomPercent = BoredomPercent + 0.1
            lblBoredom.Text = BoredomPercent.ToString("0.0")
        End If

        If (BoredomPercent = 10.0) Then
            lblBoredom.Image = My.Resources.BoredomBar_10
            lblOutput.Text &= vbCrLf & "You are getting kind of bored, maybe you should find something to do."
        ElseIf (BoredomPercent = 20.0) Then
            lblBoredom.Image = My.Resources.BoredomBar_20
        ElseIf (BoredomPercent = 30.0) Then
            lblBoredom.Image = My.Resources.BoredomBar_30
        ElseIf (BoredomPercent = 40.0) Then
            lblBoredom.Image = My.Resources.BoredomBar_40
        ElseIf (BoredomPercent = 50.0) Then
            lblBoredom.Image = My.Resources.BoredomBar_50
            lblOutput.Text &= vbCrLf & "Half way there! (there = death)"
        ElseIf (BoredomPercent = 60.0) Then
            lblBoredom.Image = My.Resources.BoredomBar_60
        ElseIf (BoredomPercent = 70.0) Then
            lblBoredom.Image = My.Resources.BoredomBar_70
        ElseIf (BoredomPercent = 80.0) Then
            lblBoredom.Image = My.Resources.BoredomBar_80
        ElseIf (BoredomPercent = 90.0) Then
            lblBoredom.Image = My.Resources.BoredomBar_90
        ElseIf (BoredomPercent = 100.0) Then
            lblBoredom.Image = My.Resources.BoredomBar_100
            If lblBoredom.Text = "100.0" Then
                lblOutput.Text &= vbCrLf & "You have died of boredom. GAME OVER!"
            End If
            If Status = 1 Then
                Me.BackgroundImage = My.Resources.Fire
            End If
            If Status = 2 Then
                Me.BackgroundImage = My.Resources.Trippy
            End If
            If Status = 3 Then
                Me.BackgroundImage = My.Resources.Trippy
                lblOutput.Image = My.Resources.steveCat
            End If
            If Status = 4 Then
                lblOutput.Image = My.Resources.Doll
            End If
            BoredomTimer.Dispose()
            txtInput.Enabled = False
            btnSubmit.Enabled = False
        End If
        scr += 1
        If (scr = 10) And (WIN = False) Then
            scr = 0
            ScrollText()
        End If
    End Sub

    Private Function getCurRoom()
        If (Position.Floor = 0) Then
            Return Data.Basement(Position.Row, Position.Column)
        ElseIf (Position.Floor = 1) Then
            Return Data.FirstFloor(Position.Row, Position.Column)
        ElseIf (Position.Floor = 2) Then
            Return Data.SecondFloor(Position.Row, Position.Column)
        End If
        Return Data.FirstFloor(Position.Row, Position.Column)
    End Function

    Private Sub enterNewRoom(ByVal direction As Integer)
        If (direction = 1) Then
            Position.Row -= 1
        ElseIf (direction = 2) Then
            Position.Column -= 1
        ElseIf (direction = 3) Then
            Position.Row += 1
        ElseIf (direction = 4) Then
            Position.Column += 1
        End If
        curRoom = getCurRoom()
        If (hasLight) Then
            lblOutput.Text &= vbCrLf & curRoom.Description & vbCrLf & DisplayExits()
        Else
            lblOutput.Text &= vbCrLf & "You stumble into that direction."
            If start Then
                lblOutput.Text &= "  You trip over something lying on the ground."
                start = False
                picFlashlight.Visible = True
            End If
        End If
    End Sub

    Private Sub enterNewFloor()
        If ((Position.Row = 3) And (Position.Column = 3)) And (Position.Floor = 1) Then
            Position.Floor = 0
            Position.Row = 1
            Position.Column = 3
        ElseIf ((Position.Row = 4) And (Position.Column = 3)) And (Position.Floor = 1) Then
            Position.Floor = 2
            Position.Row = 4
            Position.Column = 3
        ElseIf ((Position.Row = 4) And (Position.Column = 3)) And (Position.Floor = 2) Then
            Position.Floor = 1
            Position.Row = 4
            Position.Column = 3
        ElseIf ((Position.Row = 1) And (Position.Column = 3)) And (Position.Floor = 0) Then
            Position.Floor = 1
            Position.Row = 3
            Position.Column = 3
        End If
        curRoom = getCurRoom()
        If (hasLight) Then
            lblOutput.Text &= vbCrLf & curRoom.Description & vbCrLf & DisplayExits()
        Else
            lblOutput.Text &= vbCrLf & "As you move through the dark, you take a step forward and realize you found the stairs.  You awaken a bit later in a great deal of pain."
        End If
    End Sub

    Private Function isDirectionValid(ByVal direction As Integer)
        Dim alexisabastard As Boolean = False
        If ((direction = 1) And (curRoom.exits(1) = True)) Then
            alexisabastard = True
        ElseIf ((direction = 2) And (curRoom.exits(2) = True)) Then
            alexisabastard = True
        ElseIf ((direction = 3) And (curRoom.exits(3) = True)) Then
            alexisabastard = True
        ElseIf ((direction = 4) And (curRoom.exits(4) = True)) Then
            alexisabastard = True
        End If
        Return alexisabastard
    End Function

    Private Function isStairsValid()
        Dim s As Boolean
        If (curRoom.hasStairs = True) Then
            s = True
        Else
            s = False
        End If
        Return s
    End Function

    Private Function DisplayExits()
        Dim e As String = "Possible Exits: "
        If curRoom.exits(1) Then
            e &= "North "
        End If
        If curRoom.exits(2) Then
            e &= "West "
        End If
        If curRoom.exits(3) Then
            e &= "South "
        End If
        If curRoom.exits(4) Then
            e &= "East "
        End If
        If curRoom.hasStairs Then
            e &= "Stairs "
        End If
        Return e
    End Function

    Public Sub ScrollText()
        Dim tex As String = ""
        Dim line As Integer = 0
        tex = lblOutput.Text
        line = tex.IndexOf((Chr(13) + Chr(10)))
        If (line > -1) Then
            tex = tex.Remove(0, line + 2)
            lblOutput.Text = tex
        End If
    End Sub

    Private Sub WinTimer_Tick(sender As System.Object, e As System.EventArgs) Handles WinTimer.Tick
        BoredomTimer.Dispose()
        BoredomPercent = 0
        lblBoredom.Visible = False
        picFlashlight.Visible = False
        btnSubmit.Visible = False
        txtInput.Visible = False
        picInventory0.Visible = False
        picInventory1.Visible = False
        picInventory2.Visible = False
        picInventory3.Visible = False
        picInventory4.Visible = False
        picInventory5.Visible = False
        picInventory6.Visible = False
        picInventory7.Visible = False
        picInventory8.Visible = False
        picInventory9.Visible = False
        lblOutput.ForeColor = Color.Black
        lblOutput.Size = New System.Drawing.Size(663, 409)
        lblOutput.Image = My.Resources.MeGusta
        lblOutput.TextAlign = ContentAlignment.TopCenter
        Me.BackColor = Color.LightBlue
        lblOutput.Text = vbCrLf & "THE POWER IS BACK ON. YAY YOU DIDN'T DIE OF BOREDOM. GAME-THE FUCK-OVER!"

    End Sub
End Class
