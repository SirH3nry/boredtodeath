BoredToDeath
============
Code and design by Alexander Yochim, Shane Shafferman, Steve Chalker

A "boredom" themed text adventure game made in a few days for Penn State Behrend Game Development Club.  In the game, the power goes out and the player must race against the clock, which happens to be the "boredom bar." If the bar reaches 100%, the game is over. The player must use common text adventure commands to navigate his house in search of items to ease his boredom. If the player survives until the lights come back on, they win.
